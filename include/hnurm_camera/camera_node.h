//
// Created by Kai Wang on 24-4-3.
//

#ifndef HNURM_CAMERA_NODE_H
#define HNURM_CAMERA_NODE_H

#include "mvs/camera.h"

#include <controller_manager_msgs/srv/configure_controller.hpp>
#include <rclcpp/rclcpp.hpp>
#include <sensor_msgs/msg/image.hpp>
#include <std_srvs/srv/trigger.hpp>
#include <sensor_msgs/msg/camera_info.hpp>
#include <sensor_msgs/image_encodings.hpp>

class CameraNode : public rclcpp::Node
{
public:
    CameraNode() : Node("camera_node")
    {
        publisher_img_ = this->create_publisher<sensor_msgs::msg::Image>("img_raw", 10);
        publisher_camInfo_ = this->create_publisher<sensor_msgs::msg::CameraInfo>("camera_info",10);
        timer_ = this->create_wall_timer(std::chrono::milliseconds(30), std::bind(&CameraNode::publish_message, this));
        /*
        capture_thread_ = std::thread([this](){
            while(rclcpp::ok()){
                publish_message();
            }
        });
         */
        service_open_ = this->create_service<controller_manager_msgs::srv::ConfigureController>(
            "open", std::bind(&CameraNode::open, this, std::placeholders::_1, std::placeholders::_2)
        );
        service_close_ = this->create_service<std_srvs::srv::Trigger>(
            "close", std::bind(&CameraNode::close, this, std::placeholders::_1, std::placeholders::_2)
        );

        this->declare_parameter("nPixelType", rclcpp::ParameterType::PARAMETER_STRING);
        this->declare_parameter("nPixelFormat", rclcpp::ParameterType::PARAMETER_INTEGER);
        this->declare_parameter("GainAuto", rclcpp::ParameterType::PARAMETER_INTEGER);
        this->declare_parameter("nWidth", rclcpp::ParameterType::PARAMETER_INTEGER);
        this->declare_parameter("nHeight", rclcpp::ParameterType::PARAMETER_INTEGER);
        this->declare_parameter("nOffsetX", rclcpp::ParameterType::PARAMETER_INTEGER);
        this->declare_parameter("nOffsetY", rclcpp::ParameterType::PARAMETER_INTEGER);
        // this->declare_parameter("AcquisitionFrameRate",false);
        this->declare_parameter("ExposureTime", rclcpp::ParameterType::PARAMETER_DOUBLE);
        this->declare_parameter("Gain", rclcpp::ParameterType::PARAMETER_DOUBLE);

        callback_handle_
            = this->add_on_set_parameters_callback(std::bind(&CameraNode::param_callback, this, std::placeholders::_1));
    }

private:
    // 实际发布消息的地方
    void publish_message();

    // 服务
    void open(
        std::shared_ptr<controller_manager_msgs::srv::ConfigureController_Request>  request,
        std::shared_ptr<controller_manager_msgs::srv::ConfigureController_Response> response
    );

    void close(
        std::shared_ptr<std_srvs::srv::Trigger_Request>  request,
        std::shared_ptr<std_srvs::srv::Trigger_Response> response
    );

    // 发布者和定时器的声明
    rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr publisher_img_;
    rclcpp::Publisher<sensor_msgs::msg::CameraInfo>::SharedPtr publisher_camInfo_;
    rclcpp::TimerBase::SharedPtr                          timer_;
    //std::thread capture_thread_;

    // 服务声明
    rclcpp::Service<controller_manager_msgs::srv::ConfigureController>::SharedPtr service_open_;
    rclcpp::Service<std_srvs::srv::Trigger>::SharedPtr                            service_close_;

    // 参数回调函数
    rclcpp::Node::OnSetParametersCallbackHandle::SharedPtr callback_handle_;
    rcl_interfaces::msg::SetParametersResult param_callback(const std::vector<rclcpp::Parameter> &parameters);

    // 声明相机
    Camera cam_;
};

#endif  // HNURM_CAMERA_NODE_H
