# 海康工业相机ROS2驱动程序

## 使用
1. 克隆仓库到工作空间的`src`目录下
```bash
cd <your_ws>/src
git clone --recurse-submodules https://gitee.com/hnuyuelurm/hnurm_camera.git
```
2. 编译
```bash
cd <your_ws>
colcon build --packages-select hnurm_camera 
```
3. 启动相机节点
```bash
source install/setup.bash # sh or zsh 
ros2 launch hnurm_camera hnurm_camera.launch.py
```
或者
```bash
ros2 run hnurm_camera hnurm_camera_node
```
4. 打开相机
```bash
ros2 service call /open controller_manager_msgs/srv/ConfigureController <guid>
```
打开之后就会开始发送图像数据，可以使用
```bash
ros2 launch foxglove_bridge foxglove_bridge_launch.xml
```
在foxglove上看到图像
5. 关闭相机
```bash
ros2 service call /close std_srvs/srv/Trigger 
```
## Todo
### 设计
- [ ] Camera类和ROS2依赖解耦合

### 参数设置
- [x] 统一使用`Camera::set_params`方法传入参数名和参数值，返回设置状态
- [x] 通过设置`guid`打开指定相机
### 采集
- [x] 利用`MV_CC_RegisterImageCallBackEx`， 实现回调取图
- [x] 利用`MV_CC_GetImageBuffer`，实现主动取图

## 测试
- [ ] 增加测试
- [ ] 启动参数文件
- [ ] 线程
- [ ] 消息cameraInfo参数设置
## 最终效果
- ros2 launch hnurm_camera <launch文件>之后相机以默认参数启动 
- 启动后可以使用ros2 param set来设置相机参数
- 用软件订阅camera_node，查看相机获得的图像
