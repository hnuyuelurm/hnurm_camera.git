//
// Created by Kai Wang on 24-4-3.
//

#include "hnurm_camera/camera_node.h"

std::string encoding_type(MvGvspPixelType type)
{
    switch(type)
    {
    case PixelType_Gvsp_BayerGB8:
        return "bayer_bggr8";  // 不是bayer_grbg8、bayer_rggb8、bayer_gbrg8、bayer_bggr8
    case PixelType_Gvsp_BayerRG8:
        return "bayer_gbrg8";
    case PixelType_Gvsp_BayerBG8:
        return "bayer_grbg8";
    case PixelType_Gvsp_Mono8:
        return "mono8";
    case PixelType_Gvsp_RGB8_Packed:
        return "rgb8";
    case PixelType_Gvsp_BGR8_Packed://
        return "bgr8";
    case PixelType_Gvsp_RGBA8_Packed:
        return "rgba8";
    case PixelType_Gvsp_BGRA8_Packed:
        return "bgra8";
    case PixelType_Gvsp_Mono16:
        return "mono16";
    case PixelType_Gvsp_RGB16_Packed:
        return "rgb16";

    default:
        return "NULL";
    }
}

void CameraNode::publish_message()
{
    if(cam_.is_opened())
    {

        std::vector<uint8_t> img;
        struct image_info     imageInfo;
        std::string          what;

        // get dstImageInfo from camera_node params
        struct image_info     dstImageInfo;
        int type;
        get_parameter<int>("nPixelType",type);
        dstImageInfo.type  = (MvGvspPixelType)type;
        get_parameter<unsigned short>("nWidth",dstImageInfo.width);
        get_parameter<unsigned short>("nHeight",dstImageInfo.height);
        // convert
        int ret = cam_.get_frame(&imageInfo,img,what,&dstImageInfo);

        // 发布图像信息
        if(ret)
        {

            sensor_msgs::msg::Image      msg;
            sensor_msgs::msg::CameraInfo cameraInfo;
            // msg

            //RCLCPP_INFO(this->get_logger(), "the type is %s", msg.encoding.c_str());

            /*std::cout << "图像数据长度：" << msg.data.size() << std::endl;
            std::cout << std::endl;*/

            msg.data            = img;
            msg.header.stamp    = now();
            msg.header.frame_id = "camera";
            msg.encoding        = encoding_type(imageInfo.type);//sensor_msgs::image_encodings::BGR8
            msg.height          = imageInfo.height;
            msg.width           = imageInfo.width;
            msg.is_bigendian    = 0;
            msg.step            = imageInfo.width * 3;
            // cameraInfo
            cameraInfo.width = imageInfo.height;
            cameraInfo.width = imageInfo.width;
            get_parameter<uint32_t>("nOffsetX",cameraInfo.roi.width);
            get_parameter<uint32_t>("nOffsetY",cameraInfo.roi.height);


            publisher_img_->publish(msg);
            // publisher_camInfo_->publish(cameraInfo);
        }
        else
        {
            // 日志记录
            RCLCPP_ERROR(this->get_logger(), "Publishing:%s", what.c_str());
        }
    }
}

void CameraNode::open(
    const std::shared_ptr<controller_manager_msgs::srv::ConfigureController_Request> request,
    std::shared_ptr<controller_manager_msgs::srv::ConfigureController_Response>      response
)
{
    RCLCPP_INFO(this->get_logger(), "Incoming request:open camera %s", request->name.c_str());
    std::string what;
    int ret = cam_.open_camera(what, request->name);
    if(ret)
    {
        response->ok = true;
        RCLCPP_INFO(this->get_logger(), "Sending back response:%s", what.c_str());
    }
    else
    {
        response->ok = false;
        RCLCPP_ERROR(this->get_logger(), "Service_open:%s", what.c_str());
    }
}

void CameraNode::close(
    const std::shared_ptr<std_srvs::srv::Trigger_Request> request,
    std::shared_ptr<std_srvs::srv::Trigger_Response>      response
)
{
    RCLCPP_INFO(this->get_logger(), "Incoming request:close camera");
    std::string what;
    int         ret = cam_.close_camera(what);
    if(ret)
    {
        response->success = true;
        response->message = "close successfully";
        RCLCPP_INFO(this->get_logger(), "Sending back response:%s", response->message.c_str());
    }
    else
    {
        response->success = false;
        response->message = what;
        RCLCPP_ERROR(this->get_logger(), "Service_close:%s", what.c_str());
    }
}

rcl_interfaces::msg::SetParametersResult CameraNode::param_callback(const std::vector<rclcpp::Parameter> &parameters)
{
    rcl_interfaces::msg::SetParametersResult result;
    result.successful = true;
    std::string what;
    for(const auto &parameter : parameters)
    {

        if(!cam_.set_params(parameter.get_name(), 7, what))
        {
            result.successful = false;
            result.reason     = what;
        }
    }
    return result;
}

int main(int argc, char **argv)
{
    rclcpp::init(argc, argv);
    auto camera_node = std::make_shared<CameraNode>();
    rclcpp::spin(camera_node);
    rclcpp::shutdown();
    return 0;
}